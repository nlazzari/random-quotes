var QuoteModule = (function(){

	// private array of quote objects:
	// {quote: "...", author: "...""}
	var _quoteList = [
		{q: "Life has no meaning a priori. It is up to you to give it a meaning, and value is nothing but the meaning that you choose." , a: "Jean-Paul Sartre"}, 
		{q: "The universe seems neither benign nor hostile, merely indifferent.", a: "Carl Sagan"}, 
		{q: "Remembering that you are going to die is the best way I know to avoid the trap of thinking you have something to lose. You are already naked. There is no reason not to follow your heart.", a: "Steve Jobs"}, 
		{q: "Failure is always an option. Failure is the most readily available option at all times, but it's a choice. You can choose to fail or you can choose to succeed.", a: "Chael Sonnen"}, 
		{q: "A witty saying proves nothing.", a: "Voltaire"}, 
		{q: "I feel like my body is a station wagon in which I drive my brain around, like a suburban mother taking the kids to hockey practice." , a: "Douglas Coupland, Microserfs"}, 
		{q: "Aw, I have three kids and no money. Why can't I have no kids and three money?" , a: "Homer Simpson"},
		{q: "Why don't you try...Moe's hobo chicken chili. I start with the best part - the neck - and then I add secret hobo spices.", a: "Moe Szyslak"},
		{q: "Human sacrifice! Dogs and cats, living together! Mass hysteria!", a: "Bill Murray, Ghostbusters"},
		{q: "A man with priorities so far out of whack doesn't deserve such a fine automobile.", a: "Ferris Bueller" },
		{q: "I'm picking out a Thermos for you. Not an ordinary Thermos for you. But the extra best Thermos that you can buy, with vinyl and stripes and a cup built right in.", a: "Steve Martin, The Jerk" },
		//{q: "", a: ""},	

	];

	// private array of background image quote URLs
	// to accompany the quotes
	var _imgList = [
		"http://nlazzari.com/quotes/img/68ZTusF.jpg",
		"http://nlazzari.com/quotes/img/D79POdW.jpg",
		"http://nlazzari.com/quotes/img/rRuCLLC.jpg",
		"http://nlazzari.com/quotes/img/D4AhlBK.jpg",
		"http://nlazzari.com/quotes/img/R6o2zy4.jpg",
		"http://nlazzari.com/quotes/img/CoquCPk.jpg",
		"http://nlazzari.com/quotes/img/cUpjCsO.jpg",
		"http://nlazzari.com/quotes/img/IJEU2Ai.jpg",
		"http://nlazzari.com/quotes/img/TBDJZCt.jpg",
		"http://nlazzari.com/quotes/img/7Ql6qzu.jpg",
		"http://nlazzari.com/quotes/img/ecogEGZ.jpg",
		"http://nlazzari.com/quotes/img/oe0yeUG.jpg",
		"http://nlazzari.com/quotes/img/rflphed.jpg"
	];

	// Returns a random quote
	var getQuote = function() {
		return _quoteList[ Math.floor( Math.random() * _quoteList.length ) ] ;
	};

	// Returns a random background image URL
	var getImg = function() {
		return _imgList[ Math.floor( Math.random() * _imgList.length ) ] ;
	}

	// Random quote
	var rand = getQuote();

	//Quote object initialized to a random quote
	// and background image
	var quote = {
		quote: rand.q,
		author: rand.a,
		img: getImg()
	};


	//Updates the quote object to a new random quote
	quote.randomize = function() {
		var r = getQuote();
		
		this.quote = r.q;
		this.author = r.a;
		this.img = getImg();
	};

	return quote;



	}());

console.log(	QuoteModule.quote	);
console.log(	QuoteModule.author	);
console.log(	QuoteModule.img	);



